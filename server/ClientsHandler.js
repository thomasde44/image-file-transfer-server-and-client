//==========================================================================
// imports 
//==========================================================================
let singleton = require('./Singleton');
let fs = require("fs");




// map of file extensions
var image_extension_map = {1: 'bmp', 2: 'jpeg', 3: 'gif', 4: 'png', 5: 'tiff', 6: 'raw'};
// map of request types
var request_type = {0: 'query', 1: 'found', 2: 'not found', 3: 'busy'};
// tmp data variable =
var tmp_data;
// temp time stamp for server timer
var tmp_timestamp = null;
// time stamp for server timer
var timestamp = Math.floor(Math.random() * (999 - 1) + 1);


//==========================================================================
// function of the timer to increment the count
//==========================================================================
function intervalFunc() {
    if (timestamp == 2**32-1) {
        timestamp = 0
    }
    timestamp++;
    
}
// execute timer
setInterval(intervalFunc, 1000);


module.exports = {
    //==========================================================================
    // module function to handle incoming connection request packets
    // waits for a done message from the client and ends the socket connection
    // finds the correct image requested and sends it to the client
    // the server sends the image in chunks of the 30000 bytes 
    // if the file is less than 30000 it sends the exact amount of bytes
    //==========================================================================
    handleClientJoining: function (sock) {
        tmp_timestamp = timestamp;
        console.log(sock.remoteAddress + ":" + sock.remotePort + " connected.");
        sock.on('data', data => {
            
            if (data == 'done') {
                console.log("image received")
                sock.end();
            } else {
                console.log("ITP Packet Received");
                printPacketBit(data.slice(0, 12));
                console.log('\n')
                tmp_data = data;
                let version = get_version(data);
                let request = get_request_type(data);

                if (request == 0) {

                    let file_name_size = get_file_size(data);
                    let file_extension = get_extension(data)
                    
                    let name = compute_name(data, file_name_size);
                    
                
                    let extension = image_extension_map[parseInt(file_extension)];
                    
                    
                    file_path = './images/' + name + '.' + extension;
                    
                    
                    try {
                        const file = fs.readFileSync(file_path, {encoding:'base64'});
                        var file_buffer = new Buffer.from(file, 'base64')
                        response = 1;
                        socket_send_image(sock, version, response, file_buffer, file_extension, name);
                        console.log('Client-' + timestamp + ' closed the connection.\n');
                    } catch (err) {
                        socket_send_not_found(sock, version);
                        return;
                    }
                    
                    
                }
            }
        });
    },

};


//==========================================================================
// send a not found header packet with payload and seq num of 0 meaning done
// Params: socket obj, version number
//==========================================================================
function socket_send_not_found(socket, version) {
    packet = make_header_packet(version, 2, 0, timestamp, 0)
    if (socket.write(packet) == false) {
        console.log('not sent')
    }
}

//==========================================================================
// send the image file break it into chunks of 30000 depending on the size
// Params: socket obj, version number, reques number, image file data, file extension of img, image file name
//==========================================================================
function socket_send_image(socket, version, request, file_buffer, file_extension, file_name) {

    var tmp = false;
    var difference = 0;
    var seq_num = 1;
    img_size = file_buffer.length;
    difference = img_size;
    var clock = 0;
    max_packet_size = 30000;
    response = 1

    while(tmp != true) {
        
        if(difference < 30000) {
            
            packet = make_response_packet(version, response, 0, timestamp, difference, file_buffer.slice(clock*max_packet_size, img_size));
            tmp = true;
        } else {
            
            packet = make_response_packet(version, response, seq_num, timestamp, max_packet_size, file_buffer.slice(clock*max_packet_size, seq_num*max_packet_size));
        }
        // console.log(packet)
        if (socket.write(packet) == false) {
            
        }
        
        clock++;
        difference -= 30000;
        seq_num++;

        
    }

    console.log('Client-' + timestamp + ' requests:');
    console.log('\tITP version:', version);
    console.log('\tRequest type:', request_type[request]);
    console.log('\tFile extension(s):', image_extension_map[file_extension]);
    console.log('\tFile name:', file_name);


    socket.destroy(); 
    
}
//==========================================================================
// make an empty buffer of zeros of correct length for response
//==========================================================================
function zeroes() {
    let arr = new Buffer.alloc(12).fill(0);
    return arr
}


//==========================================================================
// make response packets based on the received information from the client
// just header no payload
//==========================================================================
function make_header_packet(version, response_type, seq_num, timestamp, img_size) {
    let zero_vector = zeroes();
    // console.log(img_data);
    storeBitPacket(zero_vector, version, 4, 0);
    storeBitPacket(zero_vector, response_type, 8, 4);
    storeBitPacket(zero_vector, seq_num, 12, 20);
    storeBitPacket(zero_vector, timestamp, 32, 32);
    storeBitPacket(zero_vector, img_size, 32, 64);
    
    // storeBitPacket(zero_vector, (img_data), img_size, 96);
    return zero_vector;
}

//==========================================================================
// make response packets based on the received information from the client
//==========================================================================
function make_response_packet(version, response_type, seq_num, timestamp, img_size, img_data) {
    let zero_vector = zeroes();
    // console.log(img_data);
    storeBitPacket(zero_vector, version, 4, 0);
    storeBitPacket(zero_vector, response_type, 8, 4);
    storeBitPacket(zero_vector, seq_num, 12, 20);
    storeBitPacket(zero_vector, timestamp, 32, 32);
    storeBitPacket(zero_vector, img_size, 32, 64);
    let arr = [zero_vector, img_data]
    let buf = new Buffer.concat(arr);
    
    // storeBitPacket(zero_vector, (img_data), img_size, 96);
    return buf;
}

//==========================================================================
// mirror bits unused
//==========================================================================
function mirror_bits(n) {
    return parseInt(n.toString(2).split("").reverse().join(""), 2);
}


//==========================================================================
// get file extension
//==========================================================================
function get_extension(data) {
    return parseBitPacket(data, 64, 4);
}

//==========================================================================
// get file name from bits
//==========================================================================
function compute_name(data, size) {
    let name = "";
    for (let i = 0; i < size; i++) {
        name += String.fromCharCode(data[i+12]);
    }
    return name;
}

//==========================================================================
// get version from the packet
//==========================================================================
function get_version(data) {
    return parseBitPacket(data, 0, 4);
}

//==========================================================================
// get request type
//==========================================================================
function get_request_type(data) {
    return parseBitPacket(data, 24, 8);
}


//==========================================================================
// get time stamp
//==========================================================================
function get_time_stamp(data) {
    return parseBitPacket(data, 32, 32);
}

//==========================================================================
// get file size
//==========================================================================
function get_file_size(data) {
    return parseBitPacket(data, 68, 28);
}

//==========================================================================
// get file name
//==========================================================================
function get_file_name(data) {
    return (parseBitPacket(data, 96, 32));
}

//==========================================================================
// Feel free to use them, but DON NOT change or add any code in these methods.
// Returns the integer value of the extracted bits fragment for a given packet
//==========================================================================
function parseBitPacket(packet, offset, length) {
    let number = "";
    for (var i = 0; i < length; i++) {
        // let us get the actual byte position of the offset
        let bytePosition = Math.floor((offset + i) / 8);
        let bitPosition = 7 - ((offset + i) % 8);
        let bit = (packet[bytePosition] >> bitPosition) % 2;
        number = (number << 1) | bit;
    }
    return number;
}
//==========================================================================
// Prints the entire packet in bits format
//==========================================================================
function printPacketBit(packet) {
    var bitString = "";

    for (var i = 0; i < packet.length; i++) {
        // To add leading zeros
        var b = "00000000" + packet[i].toString(2);
        // To print 4 bytes per line
        if (i > 0 && i % 4 == 0) bitString += "\n";
        bitString += " " + b.substr(b.length - 8);
    }
    console.log(bitString);
}

//==========================================================================
// Converts byte array to string
//==========================================================================
function bytesToString(array) {
    var result = "";
    for (var i = 0; i < array.length; ++i) {
        result += String.fromCharCode(array[i]);
    }
    return result;
}
//==========================================================================
// store sequence of bits in a larger sequence of bits
//==========================================================================
function storeBitPacket(packet, value, offset, length) {
    // let us get the actual byte position of the offset
    let lastBitPosition = offset + length - 1;
    let number = value.toString(2);
    let j = number.length - 1;
    for (var i = 0; i < number.length; i++) {
        let bytePosition = Math.floor(lastBitPosition / 8);
        let bitPosition = 7 - (lastBitPosition % 8);
        if (number.charAt(j--) == "0") {
            packet[bytePosition] &= ~(1 << bitPosition);
        } else {
            packet[bytePosition] |= 1 << bitPosition;
        }
        lastBitPosition--;
    }
}


//==========================================================================
// string to bytes function returns array of bytes
//==========================================================================
function stringToBytes(str) {
    var ch,
      st,
      re = [];
    for (var i = 0; i < str.length; i++) {
      ch = str.charCodeAt(i); // get char
      st = []; // set up "stack"
      do {
        st.push(ch & 0xff); // push byte to stack
        ch = ch >> 8; // shift value down by 1 byte
      } while (ch);
      // add stack contents to result
      // done because chars have "wrong" endianness
      re = re.concat(st.reverse());
    }
    // return an array of bytes
    return re;
  }
