//==========================================================================
// import statements 
//==========================================================================
let net = require("net");
let fs = require("fs");
let open = require("open");
let crypto = require("crypto");

let Buffer = require('buffer').Buffer;

const { exit } = require("process");
const { version } = require("os");

// clients timer initialization
var timestamp = Math.floor(Math.random() * (999 - 1) + 1);
// boolean to notify image received close program
var done_flag = false;

//==========================================================================
// the clients timer is set and this is the function that exectutes it 
//==========================================================================
function intervalFunc() {

  if (timestamp == 2**32-1) {
    timestamp = 0
  }
  
  if ((tmp_timestamp + 2) == (timestamp) && !done_flag) {
    
    handle_client_transaction();
    
    console.log('timeout')
    
  }
  timestamp++;
}

// execute the timer for the client
setInterval(intervalFunc, 1000);
// map for file extensions
var image_extension_map = {'bmp': 1, 'jpeg': 2, 'gif': 3, 'png': 4, 'tiff': 5, 'raw': 6};
// req/response type
var request_type = {0: 'query', 1: 'found', 2: 'not found', 3: 'busy'};

// used for tracking timeout
var tmp_timestamp = null;
// Enter your code for the client functionality here


// Convert a given string to byte array
function stringToBytes(str) {
  var ch,
    st,
    re = [];
  for (var i = 0; i < str.length; i++) {
    ch = str.charCodeAt(i); // get char
    st = []; // set up "stack"
    do {
      st.push(ch & 0xff); // push byte to stack
      ch = ch >> 8; // shift value down by 1 byte
    } while (ch);
    // add stack contents to result
    // done because chars have "wrong" endianness
    re = re.concat(st.reverse());
  }
  // return an array of bytes
  return re;
}


//==========================================================================
// main function the server passes a thread to
// reads command line arguments  constructs a packet based on the request structure standard
// gets the image file requested from the server DB
// creates a socket and sends a request packet to the server from the command like input
// if the client doesnt hear back in 20ms it resends the request until it gets the picture
//==========================================================================
function handle_client_transaction() {
  received_image = [];
  tmp_timestamp = timestamp;

  let host_port = process.argv[3].split(":");
  let host = host_port[0];
  let port = host_port[1];
  let image_name = process.argv[5];
  let image_split = image_name.split(".");
  let image_extension = image_split[1].toLowerCase();
  resend_host = host;
  resend_port = port;
  
  

  let image_name_without_extension = image_split[0];

  let file_name_length = image_name_without_extension.length;
 
  let version = parseInt(process.argv[7]);
  
  packet = make_request_packet(version, image_extension, image_name_without_extension, file_name_length, port);
  
  let socket = new net.Socket();
  socket.connect(port, host);
  

  // get response from server
  socket.write(new Buffer.from(packet, 'binary'));
  console.log('\n')

  socket.on('data', data => {
    
    let tmp_buf = new Buffer.from(data.slice(12, data.length), 'base64')
    // received_image.push(data.slice(12, data.length));
    
    received_image.push(tmp_buf)
    version = get_version(data);
    response = get_response(data);
    seq_num = get_seq_num(data);
    time = get_time(data);
    img_size = get_img_size(data);

    
    console.log('connected to ImageDB server on ' + host + ':' + port)
    console.log('\tITP version: ', version);
    console.log('\tResponse Type = ', request_type[response]);
    console.log('\tsequence number : ', seq_num);
    console.log('\tpacket size ', img_size);
    console.log('\ttimestamp : ', time);
    console.log('\n')
    
    // seq_num means the last packet of the string of them
    if (seq_num == 0) {
      
        var newbuff = Buffer.concat(received_image);
        var new_buff = newbuff.toString('base64')
        
      fs.writeFileSync(image_name, new_buff, {encoding:'base64'}, (err) => {
        if (err) return console.error(err);
        console.log("File successfully written !");
      });
      // notify server received
      console.log('Disconnected from server');
      socket.write('done');
      done_flag = true;
      socket.end();
      
      open_async(image_name)
      
      
      exit();
    }
    
  })
  
}

async function delete_image(image_name) {
  fs.unlink(image_name, (err) => {
    if (err) {
        throw err;
    }
  
    console.log("File is deleted.");
  });
}

async function open_async(image_name) {
  await open(image_name, {wait: true});
  
}



// when program is executed this function is called
// it gets called again in the timer enclosure
handle_client_transaction()


//==========================================================================
// create a buffer filled with zeros 
//==========================================================================
function zeroed_bytes(length) {

  let buf = new Buffer(12 + length).fill(0);
  return buf
}

//==========================================================================
// make the request packet 
//==========================================================================
function make_request_packet(version, image_extension, image_name, file_name_length, time) {
  let zeros = zeroed_bytes(image_name.length);
  
  storeBitPacket(zeros, version, 4, 0);
  storeBitPacket(zeros, time, 32, 32);
  storeBitPacket(zeros, image_extension_map[image_extension], 36, 32);
  storeBitPacket(zeros, file_name_length, 68, 28);
  
  // console.log('img name', image_name)
  for (var i = 0; i < image_name.length; i++) {
    storeBitPacket(zeros, stringToBytes(image_name)[i], i * 8 + 96, 8);
  }

  return zeros;
}
//==========================================================================
// get version from received packet
//==========================================================================
function get_version(data) {
  return parseBitPacket(data, 0, 4);
}

//==========================================================================
// get response from received packet
//==========================================================================
function get_response(data) {
  return parseBitPacket(data, 4, 8);
}
//==========================================================================
// get seq num from received packet
//==========================================================================
function get_seq_num(data) {
  return parseBitPacket(data, 12, 20);
}
//==========================================================================
// get time from received packet
//==========================================================================
function get_time(data) {
  return parseBitPacket(data, 32, 32);
}
//==========================================================================
// get image size from received packet
//==========================================================================
function get_img_size(data) {
  return parseBitPacket(data, 64, 32);
}



//==========================================================================
// Feel free to use them, but DON NOT change or add any code in these methods.
// Returns the integer value of the extracted bits fragment for a given packet
//==========================================================================
function parseBitPacket(packet, offset, length) {
    let number = "";
    for (var i = 0; i < length; i++) {
      // let us get the actual byte position of the offset
      let bytePosition = Math.floor((offset + i) / 8);
      let bitPosition = 7 - ((offset + i) % 8);
      let bit = (packet[bytePosition] >> bitPosition) % 2;
      number = (number << 1) | bit;
    }
    return number;
  }
  
  // Prints the entire packet in bits format
  function printPacketBit(packet) {
    var bitString = "";
  
    for (var i = 0; i < packet.length; i++) {
      // To add leading zeros
      var b = "00000000" + packet[i].toString(2);
      // To print 4 bytes per line
      if (i > 0 && i % 4 == 0) bitString += "\n";
      bitString += " " + b.substr(b.length - 8);
    }
    console.log(bitString);
  }


  
//==========================================================================
// store sequence of bits within a larger sequence of bits
//==========================================================================
  function storeBitPacket(packet, value, offset, length) {
    // let us get the actual byte position of the offset
    let lastBitPosition = offset + length - 1;
    let number = value.toString(2);
    let j = number.length - 1;
    for (var i = 0; i < number.length; i++) {
        let bytePosition = Math.floor(lastBitPosition / 8);
        let bitPosition = 7 - (lastBitPosition % 8);
        if (number.charAt(j--) == "0") {
            packet[bytePosition] &= ~(1 << bitPosition);
        } else {
            packet[bytePosition] |= 1 << bitPosition;
        }
        lastBitPosition--;
    }
}

//==========================================================================
// convert a string to bytes
//==========================================================================
function stringToBytes(str) {
  var ch,
    st,
    re = [];
  for (var i = 0; i < str.length; i++) {
    ch = str.charCodeAt(i); // get char
    st = []; // set up "stack"
    do {
      st.push(ch & 0xff); // push byte to stack
      ch = ch >> 8; // shift value down by 1 byte
    } while (ch);
    // add stack contents to result
    // done because chars have "wrong" endianness
    re = re.concat(st.reverse());
  }
  // return an array of bytes
  return re;
}

