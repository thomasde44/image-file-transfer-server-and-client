### image file transfer


#### create custom header and packets to request and receive image files

**Request packet**
<img src="./doc_pic/request.png" >


**Response packet**
<img src="./doc_pic/response.png" >


**Usage**

```
// start server
node ./server/ImageDB.js

// client command line request
./client/node GetImage.js -s 127.0.0.1:3000 -q Rose.jpeg -v 7

```

